//
//  Person.h
//  hw01-Polymorphism
//
//  Created by Adnan Zejnilovic on 9/26/17.
//  Copyright © 2017 Toyocat. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef DEBUG
#define NSLog(FORMAT, ...) fprintf(stderr,"%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else
#define NSLog(...) {}
#endif

@interface Person : NSObject
{
    NSString* gender;
    NSString* name;
}
@property (getter=getName, setter=setName:)NSString* personName;
@property (getter=getGender, setter=setGender:)NSString* personGender;

-(id)initWithName:(NSString*) personName andGender:(NSString *)personGender;
-(void)display;
@end
