//
//  main.m
//  hw01-Polymorphism
//
//  Created by Adnan Zejnilovic on 9/26/17.
//  Copyright © 2017 Toyocat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
#import "MDCPerson.h"
#import "MDCProfessor.h"
#import "MDCStudent.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Person * johnDoe = [[Person alloc]initWithName:@"John Doe" andGender:@"Male"];
        Person *pattyBaker = [[MDCPerson alloc]initWithCampus:@"Wolfson" andRole:@"Security Officer" andName:@"Patty Baker" andGender:@"female"];
        Person *chrisCorbin = [[MDCProfessor alloc]initWithTeachingSpecialty:@"Networking" andDepartmentName:@"EnTec" andCampus:@"Wolfson" andRole:@"Professor" andName:@"Chris Corbin" andGender:@"male"];
        Person *eduardoPadron = [[MDCPerson alloc]initWithCampus:@"Wolfson" andRole:@"Administrator" andName:@"Eduardo Padron" andGender:@"male"];
        Person *adiZejnilovic = [[MDCProfessor alloc]initWithTeachingSpecialty:@"Computer Science" andDepartmentName:@"EnTec" andCampus:@"Wolfson" andRole:@"Professor" andName:@"Adi Zejnilovic" andGender:@"male"];
        Person *roselyneTelfort = [[MDCPerson alloc]initWithCampus:@"Wolfson" andRole:@"Student Advisor" andName:@"Roselyne Telfort" andGender:@"female"];
        Person *dianeBienAimee = [[MDCPerson alloc]initWithCampus:@"Wolfson" andRole:@"Administrator" andName:@"Diane Bien-Aimee" andGender:@"female"];
        Person *felixLopez = [[MDCProfessor alloc]initWithTeachingSpecialty:@"Computer Science" andDepartmentName:@"EnTec" andCampus:@"Inter-American" andRole:@"Professor" andName:@"Felix Lopez" andGender:@"male"];
        Person *nannetteBibby = [[MDCProfessor alloc]initWithTeachingSpecialty:@"Computer Science" andDepartmentName:@"EnTec" andCampus:@"Kendall" andRole:@"Professor" andName:@"Nannette Biby" andGender:@"female"];
        Person *jimWhite = [[MDCPerson alloc]initWithCampus:@"Wolfson" andRole:@"Custodian" andName:@"Jim White" andGender:@"male"];
        Person *janeDoe = [[MDCStudent alloc]initWithStudentMajor:@"Computer Science" andWithStudentClassification:@"Sophomore" andCampus:@"Wolfson" andRole:@"Student" andName:@"Jane Doe" andGender:@"female"];
        
        NSMutableArray *myPersons = [[NSMutableArray alloc]init];
        [myPersons addObject:johnDoe];
        [myPersons addObject:pattyBaker];
        [myPersons addObject:chrisCorbin];
        [myPersons addObject:eduardoPadron];
        [myPersons addObject:adiZejnilovic];
        [myPersons addObject:roselyneTelfort];
        [myPersons addObject:dianeBienAimee];
        [myPersons addObject:felixLopez];
        [myPersons addObject:nannetteBibby];
        [myPersons addObject:jimWhite];
        [myPersons addObject:janeDoe];
        
        for (Person *aPerson in myPersons){
            [aPerson display];
            NSLog(@"\n");
        }
        
        
    }
    return 0;
}
