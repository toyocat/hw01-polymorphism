//
//  MDCPerson.m
//  hw01-Polymorphism
//
//  Created by Adnan Zejnilovic on 9/26/17.
//  Copyright © 2017 Toyocat. All rights reserved.
//

#import "MDCPerson.h"

@implementation MDCPerson
-(id)initWithCampus:(NSString *)campusName
            andRole:(NSString *)roleType
            andName:(NSString *)personName
          andGender:(NSString *)personGender{
    self = [super initWithName:personName andGender:personGender];
    if (self){
        [self setCampus:campusName];
        [self setRole:roleType];
    }
    
    return self;
}

-(void)display{
    [super display];
    NSLog(@"%@, %@,",[self getCampus], [self getRole]);
}
@end
