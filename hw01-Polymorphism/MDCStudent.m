//
//  MDCStudent.m
//  hw01-Polymorphism
//
//  Created by Adnan Zejnilovic on 9/26/17.
//  Copyright © 2017 Toyocat. All rights reserved.
//

#import "MDCStudent.h"

@implementation MDCStudent
-(id)initWithStudentMajor:(NSString *)major andWithStudentClassification:(NSString *)classification andCampus:(NSString *)campusName andRole:(NSString *)roleType andName:(NSString *)personName andGender:(NSString *)personGender{
    self = [super initWithCampus:campusName andRole:roleType andName:personName andGender:personGender];
    if (self){
        [self setMajor:major];
        [self setClassification:classification];
    }
    
    return self;
}

-(void)display{
    [super display];
    NSLog(@"%@, %@",[self getMajor],[self getClassification]);
}
@end
