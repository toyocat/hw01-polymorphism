//
//  MDCProfessor.h
//  hw01-Polymorphism
//
//  Created by Adnan Zejnilovic on 9/26/17.
//  Copyright © 2017 Toyocat. All rights reserved.
//

#import "MDCPerson.h"

@interface MDCProfessor : MDCPerson
{
    NSString* teachingSpecialty;
    NSString* departmentName;
}

@property(getter=getTeachingSpecialty, setter=setTeachingSpecialty:)NSString* specialty;
@property(getter=getDepartment, setter=setDepartmentName:)NSString* department;

-(id)initWithTeachingSpecialty:(NSString*)specialty
             andDepartmentName:(NSString*)department
                     andCampus:(NSString *)campusName
                       andRole:(NSString *)roleType
                       andName:(NSString*)personName
                     andGender:(NSString*)personGender;
-(void)display;
@end

