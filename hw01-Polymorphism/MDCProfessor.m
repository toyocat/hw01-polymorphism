//
//  MDCProfessor.m
//  hw01-Polymorphism
//
//  Created by Adnan Zejnilovic on 9/26/17.
//  Copyright © 2017 Toyocat. All rights reserved.
//

#import "MDCProfessor.h"

@implementation MDCProfessor
-(id)initWithTeachingSpecialty:(NSString *)specialty andDepartmentName:(NSString *)department andCampus:(NSString *)campusName andRole:(NSString *)roleType andName:(NSString *)personName andGender:(NSString *)personGender{
    self = [super initWithCampus:campusName andRole:roleType andName:personName andGender:personGender];
    if (self){
        [self setTeachingSpecialty:specialty];
        [self setDepartmentName:department];
    }
    
    return self;
}

-(void)display{
    [super display];
    NSLog(@"%@, %@",[self getTeachingSpecialty], [self getDepartment]);
}
@end
