//
//  MDCPerson.h
//  hw01-Polymorphism
//
//  Created by Adnan Zejnilovic on 9/26/17.
//  Copyright © 2017 Toyocat. All rights reserved.
//

#import "Person.h"

@interface MDCPerson : Person
{
    NSString* campus;
    NSString* role;

}

@property(getter=getCampus, setter=setCampus:)NSString* campusName;
@property(getter=getRole, setter=setRole:)NSString* roleType;

-(id)initWithCampus:(NSString *)campusName
            andRole:(NSString *)roleType
            andName:(NSString*)personName
          andGender:(NSString*)personGender;
-(void)display;
@end
