//
//  MDCStudent.h
//  hw01-Polymorphism
//
//  Created by Adnan Zejnilovic on 9/26/17.
//  Copyright © 2017 Toyocat. All rights reserved.
//

#import "MDCPerson.h"

@interface MDCStudent : MDCPerson
@property(strong, nonatomic, getter=getMajor, setter=setMajor:)NSString* studentMajor;
@property(strong, nonatomic, getter=getClassification, setter=setClassification:)NSString* classification;

-(id)initWithStudentMajor:(NSString* )major
    andWithStudentClassification:(NSString* )classification
                       andCampus:(NSString *)campusName
                         andRole:(NSString *)roleType
                         andName:(NSString*)personName
                       andGender:(NSString*)personGender;

-(void)display;
@end
