//
//  Person.m
//  hw01-Polymorphism
//
//  Created by Adnan Zejnilovic on 9/26/17.
//  Copyright © 2017 Toyocat. All rights reserved.
//

#import "Person.h"

@implementation Person
-(id)initWithName:(NSString *)personName andGender:(NSString *)personGender{
    [self setName:personName];
    [self setGender:personGender];
    return self;
}

-(void)display{
   NSLog(@"%@, %@", [self getName], [self getGender]);
}
@end
